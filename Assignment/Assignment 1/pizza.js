const mysql = require('mysql')
const express = require('express')

function getPizzaInfo() {

    const connect1 = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'mydb',
        port : 3306
    })
    connect1.connect()

    const statement = 'select ID, Name, Type, Category, Description from PIZZA_ITEMS'

    connect1.query(statement, (error, data) => {
        console.log(`Error = ${error}`)
        console.log(`Data = ${data}`)
        
        console.table(data)

        connect1.end()
    })
}

// getPizzaInfo()

function addPizza(name, type, category, description) {
    
    const connect1 = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'manager',
        database: 'mydb',
        port: 3306
    })
    connect1.connect()

    const statement = `insert into PIZZA_ITEMS(Name,Type,Category, Description) values('${name}', '${type}', '${category}','${description}')`

    connect1.query(statement, (error, data) => {
        console.log(`Error = ${error}`)
        console.log(`Data = ${data} <br> Pizza added`)

        connect1.end()
    })

}

//addPizza("Cheese", "Small", "Soft Crust", "This is Delicious Pizza")