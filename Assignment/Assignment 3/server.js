const express = require('express')
const bodyParser = require('body-parser')

const routerCustomer = require('./routes/Customer')
const routerPIZZA_ITEMS = require('./routes/PIZZA_ITEMS')

const app = express()

app.use(bodyParser.json())

app.use('/Customer', routerCustomer)
app.use('/PIZZA_ITEMS', routerPIZZA_ITEMS)


app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})