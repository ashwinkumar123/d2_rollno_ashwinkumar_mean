const express = require('express')

//adding db and utils module
const db = require('../db')
const utils = require('../utils')

const router = express.Router()


//GET METHOD
router.get('/', (request,response) => {

    const ID = request.body.ID
    const statement = `select * from PIZZA_ITEMS where ID = '${ID}'`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})


//POST METHOD
router.post('/', (request,response) => {

    const Name = request.body.Name
    const Type = request.body.Type
    const Category = request.body.Category
    const Description = request.body.Decsription
    
    const statement = `insert into PIZZA_ITEMS (Name,Type,Category,Description) values ('${Name}','${Type}','${Category}','${Description}')`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})


//PUT METHOD
router.put('/', (request,response) => {

    const Category = request.body.Category
    const ID = request.body.ID
    
    const statement = `update PIZZA_ITEMS set Category = '${Category}' where ID = '${ID}'`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})




router.delete('/', (request,response) => {

    const ID = request.body.ID
    const statement = `delete from PIZZA_ITEMS where ID = '${ID}'`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})


module.exports = router