const express = require('express')

const db = require('../db')
const utils = require('../utils')

const router = express.Router()


//GET METHOD
router.get('/', (request,response) => {
    const ID = request.body.ID
    const statement = `select * from Customer where ID = '${ID}'`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})



//POST METHOD
router.post('/', (request,response) => {
    const Name = request.body.Name
    const Password = request.body.Password
    const Mobile = request.body.Mobile
    const Address = request.body.Address
    const Email = request.body.Email
    const statement = `insert into Customer (Name,Password,Mobile,Address,Email)
    values ('${Name}','${Password}','${Mobile}','${Address}','${Email}')`

    db.query(statement,(error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})



//PUT METHOD
router.put('/', (request,response) => {

    const ID = request.body.ID
    const Address = request.body.Address
    const Email = request.body.Email
    const statement = `update Customer set Address = '${Address}' ,Email = '${Email}' where ID = '${ID}'` 
    db.query(statement,(error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})


//DELETE METHOD
router.delete('/', (request,response) => {

    const ID  = request.body.ID
    const statement = `delete from Customer where ID  = '${ID}'`
    db.query(statement,(error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })
})


module.exports = router
