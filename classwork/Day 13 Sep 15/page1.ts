class Person {
    // implicit property declaration
    name
  
    // explicit property declaration
    age: number
    email: string
    address: string
  }
  
  const p1 = new Person()
  p1.name = "person1"
  p1.age = 20
  p1.address = "pune"
  p1.email = "person1@test.com"
  console.log(p1)