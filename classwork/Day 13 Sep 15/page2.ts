class Mobile {
    private model: string
    private company: string
    private price: number
  
    // getter
    public getModel(): string {
      return this.model
    }
  
    public getCompany(): string {
      return this.company
    }
  
    public getPrice(): number {
      return this.price
    }
  
    // setter
    public setModel(model: string) {
      this.model = model
    }
  
    public setCompany(company: string) {
      this.company = company
    }
  
    public setPrice(price: number) {
      this.price = price
    }
  
  }
  
  // const m1 = new Mobile()
  // m1.model = "iphone"
  // m1.company = "Apple"
  // m1.price = 144000
  // console.log(m1)
  
  const m2 = new Mobile()
  m2.setCompany("Samsung")
  m2.setModel("M30S")
  m2.setPrice(16000)
  console.log(m2)
  
  // console.log(m2.getPrice)