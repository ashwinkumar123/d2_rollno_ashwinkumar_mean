abstract class Person {
  name: string
  age: number

  constructor(name: string, age: number) {
    this.name = name
    this.age = age
  }

  public printInfo() {
    console.log(`name: ${this.name}`)
    console.log(`age: ${this.age}`)
  }

  // incomplete
  // subclass MUST implement this method
  public abstract printProfession()

}

class Player extends Person {
  team: string

  constructor(name: string, age: number, team: string) {
    super(name, age)
    this.team = team
  }

  public printProfession() {
    console.log('profession: player')
  }

}

class Developer extends Person {
  company: string

  constructor(name: string, age: number, company: string) {
    super(name, age)
    this.company = company
  }

  public printProfession() {
    console.log(`profession = development`)
  }

}

// can not create an object of abstract class
// const person1 = new Person('person1', 20)
// person1.printInfo()

const player1 = new Player('player1', 25, 'team india')
player1.printInfo()
player1.printProfession()

const d1 = new Developer('dev 1', 30, 'company 1')
d1.printInfo()
d1.printProfession()