var Person = /** @class */ (function () {
    // public constructor() {
    //   console.log("inside constructor")
    //   this._name = ''
    //   this._age = 0
    // }
    // constructor
    function Person(name, age) {
        if (age === void 0) { age = 0; }
        this._name = name;
        this._age = age;
    }
    Object.defineProperty(Person.prototype, "name", {
        // getter
        get: function () { return this._name; },
        // setter
        set: function (name) { this._name = name; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () { return this._age; },
        set: function (age) { this._age = age; },
        enumerable: false,
        configurable: true
    });
    // facilitator or utility
    Person.prototype.canVote = function () {
        if (this._age >= 18) {
            console.log(this._name + " is eligible for voting");
        }
        else {
            console.log(this._name + " is NOT eligible for voting");
        }
    };
    return Person;
}());
var p1 = new Person("person1", 20);
// p1.name = "person1"
// p1.age = 20
console.log("name = " + p1.name);
console.log("age = " + p1.age);
p1.canVote();
var p2 = new Person("person2");
// p2.name = "person2"
p2.age = 10;
console.log("name = " + p2.name);
console.log("age = " + p2.age);
p2.canVote();
