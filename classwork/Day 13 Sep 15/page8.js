var Animal = /** @class */ (function () {
    function Animal() {
    }
    Animal.prototype.run = function () {
        console.log('animal is running');
    };
    return Animal;
}());
var Rectangle = /** @class */ (function () {
    function Rectangle() {
    }
    Rectangle.prototype.draw = function () {
        console.log('drawing rectangle');
    };
    Rectangle.prototype.erase = function () {
        console.log('erasing rectangle');
    };
    Rectangle.prototype.rectangle = function () {
        console.log('inside rectangle');
    };
    return Rectangle;
}());
var Circle = /** @class */ (function () {
    function Circle() {
    }
    Circle.prototype.draw = function () {
        console.log('drawing circle');
    };
    Circle.prototype.erase = function () {
        console.log('erasing circle');
    };
    return Circle;
}());
var Square = /** @class */ (function () {
    function Square() {
    }
    Square.prototype.draw = function () {
        console.log('drawing square');
    };
    Square.prototype.erase = function () {
        console.log('erasing square');
    };
    return Square;
}());
//       type of reference = type of object
var drawable1 = new Rectangle();
drawable1.draw();
drawable1.erase();
var drawable2 = new Circle();
drawable2.draw();
drawable2.erase();
var drawable3 = new Square();
drawable3.draw();
drawable3.erase();
