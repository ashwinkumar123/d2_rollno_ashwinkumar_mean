const express = require('express')

const router = express.Router()

router.get('/', (request, response) => {
  console.log('inside GET /category')
  response.send('GET /category')
})

router.post('/', (request, response) => {
  console.log('inside POST /category')
  response.send('POST /category')
})

router.put('/', (request, response) => {
  console.log('inside PUT /category')
  response.send('PUT /category')
})

router.delete('/', (request, response) => {
  console.log('inside DELETE /category')
  response.send('DELETE /category')
})

module.exports = router