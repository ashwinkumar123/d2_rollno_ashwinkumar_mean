const express = require('express')

const router = express.Router()

router.get('/', (request, response) => {
  console.log('inside GET /note')
  response.send('GET /note')
})

router.post('/', (request, response) => {
  console.log('inside POST /note')
  response.send('POST /note')
})

router.put('/', (request, response) => {
  console.log('inside PUT /note')
  response.send('PUT /note')
})

module.exports = router