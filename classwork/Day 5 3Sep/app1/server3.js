// require the express package
const express = require('express')

// used to parse the request body contents
const bodyParser = require('body-parser')

// require mysql for db connection
const mysql = require('mysql')


// create a server instance
const app = express()

// use body parser's json parser to parse body into json object
app.use(bodyParser.json())

// ------------------------------------------
// ------------- product routes -------------
// ------------------------------------------

app.get('/product', (request, response) => {

  // creating a mysql connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // opening the connection
  connection.connect()

  // prepare the statement
  const statement = `select id, title, price, description, category, userId, company from products`

  // execute the statement
  connection.query(statement, (error, data) => {

    // close the connection
    connection.end()

    // if any error ocurrs while executing query
    if (error) {
      response.end('error while executing query')
    } else {

      // console.log(data)

      // convert the data to string, set the content-type header 
      // and send the string data to the client
      response.send(data)

      // // convert the JSON array to string
      // const string = JSON.stringify(data)

      // // set content-type for the client
      // response.setHeader('Content-Type', 'application/json')

      // // data is array of product objects
      // response.end(string)
    }
  })

})


app.post('/product', (request, response) => {
  console.log(`body: `)
  console.log(request.body)

  // creating a mysql connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // opening the connection
  connection.connect()

  const statement = `insert into products 
      (title, description, company, price, category, userId) 
      values ('${request.body.title}', 
              '${request.body.description}',
              '${request.body.company}', 
              '${request.body.price}',
              '${request.body.category}',
              '${request.body.userId}')`

  // console.log(statement)
  connection.query(statement, (error, result) => {
    // close the connection
    connection.end()

    response.send(result)
  })
})

app.put('/product', (request, response) => {
  console.log(`body: `)
  console.log(request.body)

  // creating a mysql connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // opening the connection
  connection.connect()

  const statement = `update products 
        set title = '${request.body.title}',
        description = '${request.body.description}',
        company = '${request.body.company}',
        price = '${request.body.price}',
        category = '${request.body.category}',
        userId = '${request.body.userId}'
      where id = ${request.body.id}`

  // console.log(statement)
  connection.query(statement, (error, result) => {
    // close the connection
    connection.end()

    response.send(result)
  })
})


app.delete('/product', (request, response) => {
  console.log(`body: `)
  console.log(request.body)

  // creating a mysql connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // opening the connection
  connection.connect()

  const statement = `delete from products where id = ${request.body.id}`

  // console.log(statement)
  connection.query(statement, (error, result) => {
    // close the connection
    connection.end()

    response.send(result)
  })
})

// ------------------------------------------
// -------------- user routes ---------------
// ------------------------------------------


// listen on port 3000 (start the server)
app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})