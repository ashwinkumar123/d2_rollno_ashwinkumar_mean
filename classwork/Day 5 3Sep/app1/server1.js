const express = require('express')

const app = express()

// middleware
// the function which will be called before any of the routes
function log(request, response, next) {
  console.log('inside the log function')
  console.log(`method: ${request.method}`)
  console.log(`url: ${request.url}`)

  // next will refer the next function call
  // call the next method
  // console.log(next)
  next()
}

// app will call the log function first before executing the route handler
// 1 -> log
// 2 -> respective route handler
app.use(log)

// router for index
app.get('/', (request, response) => {
  // console.log(`method: ${request.method}`)
  // console.log(`url: ${request.url}`)
  // log(request, response)

  console.log('inside GET /')
  response.end('welcome to my REST application')
})

app.post('/', (request, response) => {
  // console.log(`method: ${request.method}`)
  // console.log(`url: ${request.url}`)
  // log(request, response)

  console.log('inside POST /')
  response.end('this is a post request')
})

app.put('/', (request, response) => {
  // console.log(`method: ${request.method}`)
  // console.log(`url: ${request.url}`)
  // log(request, response)

  console.log('inside PUT /')
  response.end('this is a put request')
})

app.delete('/', (request, response) => {
  // console.log(`method: ${request.method}`)
  // console.log(`url: ${request.url}`)
  // log(request, response)

  console.log('inside DELETE /')
  response.end('this is a delete request')
})


// ------------------------------------
// -----  product related routes ------
// ------------------------------------

app.get('/product', (request, response) => {
  console.log('inside GET /product')
  response.end('this is GET /product')
})

app.post('/product', (request, response) => {
  console.log('inside POST /product')
  response.end('this is POST /product')
})

app.put('/product', (request, response) => {
  console.log('inside PUT /product')
  response.end('this is PUT /product')
})

app.delete('/product', (request, response) => {
  console.log('inside DELETE /product')
  response.end('this is DELETE /product')
})


app.listen(3000, '0.0.0.0', () => {
  console.log('server started successfully on port 3000')
})