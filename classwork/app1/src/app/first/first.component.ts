import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  firstName = 'Steve'
  lastName = 'Jobs'

  person = {
    name : 'person1',
    address : 'pune',
    phone : '+32165135161',
    email : 'person@test.com'
  }

  constructor() { }

  ngOnInit(): void {
  }

}
