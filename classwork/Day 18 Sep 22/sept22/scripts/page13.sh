#!/bin/bash

numbers=( 1 2 3 4 5 6 7 8 9 10 )

for number in "${numbers[@]}"; do
  square=$(expr $number \* $number)
  cube=$(expr $number \* $number \* $number)

  echo "square of $number = $square" 
  echo "cube of $number = $cube" 
done