#!/bin/bash

# n1=20
# n2=10

# get the first argument
n1=$1

# get the second argument
n2=$2

# perform addtion expression
addition=$(expr $n1 + $n2)
echo "addition = $addition"

# perform subtraction expression
subtraction=$(expr $n1 - $n2)
echo "subtraction = $subtraction"

# perform multiplication expression
multiplication=$(expr $n1 \* $n2)
echo "multiplication = $multiplication"

# perform division expression
division=$(expr $n1 / $n2)
echo "division = $division"

# perform modulus expression
modulus=$(expr $n1 % $n2)
echo "modlus = $modulus"