#!/bin/bash

# n1=20
# n2=10

n1=$1
n2=$2

# equality
if [ $n1 -eq $n2 ]
then
  echo "n1 is equal to n2"
else
  echo "n1 is not equal to n2"
fi

# non equality
if [ $n1 -ne $n2 ]
then
  echo "n1 is not equal to n2"
else
  echo "n1 is equal to n2"
fi


# less than
if [ $n1 -lt $n2 ]
then
  echo "n1 is less than n2"
else
  echo "n1 is not less than n2"
fi

# less than equal to 
if [ $n1 -le $n2 ]
then
  echo "n1 is less than or equal to  n2"
else
  echo "n1 is not less or equal to than n2"
fi

# greater than
if [ $n1 -gt $n2 ]
then
  echo "n1 is greater than n2"
else
  echo "n1 is not greater than n2"
fi

# greater than equal to 
if [ $n1 -ge $n2 ]
then
  echo "n1 is greater than or equal to  n2"
else
  echo "n1 is not greater or equal to than n2"
fi