#!/bin/bash

n1="test1"
n2="test2"
n3=""


# equality
if [ $n1 = $n2 ]
then
  echo "$n1 is equal to $n2"
else
  echo "$n1 is not equal to $n2"
fi

# non equality
if [ $n1 != $n2 ]
then
  echo "$n1 is not equal to $n2"
else
  echo "$n1 is equal to $n2"
fi

# check if string is empty
if [ -z $n3 ]
then
  echo "n3 is an emptry string"
else
  echo "n3 is NOT empty"
fi