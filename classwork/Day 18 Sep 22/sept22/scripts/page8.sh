#!/bin/bash

# get number from user
num=$1

# check if the number is even or not
mod=$(expr $num % 2)

if [ $mod -eq 0 ]
then
  echo "$num is even number"
else 
  echo "$num is NOT even number"
fi

if [ $mod -ne 0 ]
then
  echo "$num is odd number"
else 
  echo "$num is NOT odd number"
fi