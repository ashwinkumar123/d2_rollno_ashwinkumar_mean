#!/bin/bash

# variable
num=100
echo "num = $num"

# array of numbers
numbers=( 10 20 30 40 50 )

echo "numbers = $numbers"
echo "numbers[0] = ${numbers[0]}"
echo "numbers[1] = ${numbers[1]}"
echo "numbers[2] = ${numbers[2]}"
echo "numbers[3] = ${numbers[3]}"
echo "numbers[4] = ${numbers[4]}"

echo "numbers = ${numbers[@]}"

# the application does not crash
# echo "numbers[5] = ${numbers[5]}"


# iterate over the numbers array
for number in "${numbers[@]}"
do
  echo "value = $number"
done


# array of strings
countries=( 'india' 'usa' 'uk' 'japan' )
echo "countries = ${countries[@]}"

for country in "${countries[@]}"; do
  echo "coutnry = $country"
done

