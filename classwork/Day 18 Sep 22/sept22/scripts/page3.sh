#!/bin/bash

# create a local variable
num=100
name="Steve Jobs"

# get the value from the variable
echo $num
echo $name

# echo "value of num = " $num
echo "value of num = $num"
echo "value of name = $name"

# discard the variable
unset num
unset name

echo "value of num = $num"
echo "value of name = $name"

