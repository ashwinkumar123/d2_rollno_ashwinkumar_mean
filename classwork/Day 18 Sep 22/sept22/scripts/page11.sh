#!/bin/bash

file=$1

# check if the file is a block device
if [ -b $file ]; then
  echo "yes, this is a block device file"
else
  echo "no, this is not a block device file"
fi

# check if the file is a chracter device
if [ -c $file ]; then
  echo "yes, this is a character device file"
else
  echo "no, this is not a character device file"
fi

# check if it is a file
if [ -f $file ]; then
  echo "yes, this is a file"
else
  echo "no, this is not a file"
fi

# check if it is a directory
if [ -d $file ]; then
  echo "yes, this is a directory"
else
  echo "no, this is not a directory"
fi

# check if file has read permission
if [ -r $file ]; then
  echo "yes, file is readable"
else
  echo "no, file is not readable"
fi

# check if file has write permission
if [ -w $file ]; then
  echo "yes, file is writeable"
else
  echo "no, file is not writeable"
fi

# check if file has execute permission
if [ -x $file ]; then
  echo "yes, file is executable"
else
  echo "no, file is not executable"
fi

# check if file is empty or not
if [ -s $file ]; then
  echo "file is not empty"
else
  echo "file is empty"
fi

# check if file exists
if [ -e $file ]; then
  echo "file exists"
else
  echo "file does exist"
fi
