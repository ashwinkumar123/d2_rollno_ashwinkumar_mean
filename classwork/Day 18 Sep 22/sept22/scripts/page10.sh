#!/bin/bash

age=34

# (age >= 20) && (age <= 60 )
if [ $age -ge 20 -a $age -le 60 ]
then
  echo "the age is between 20 and 60 so you are allowed to be in the company"
else
  echo "the age is NOT between 20 and 60 so you are NOT allowed in the company"
fi
