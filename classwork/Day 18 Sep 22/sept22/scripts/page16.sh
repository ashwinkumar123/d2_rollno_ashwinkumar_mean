#!/bin/bash

echo "taking backup"

backup_file="/tmp/backup.tar"

# delete old backup file
if [ -e $backup_file ]; then
  rm $backup_file
fi

# create a backup (tar) file
tar -cvf $backup_file $HOME

# copy the file to a server
# scp