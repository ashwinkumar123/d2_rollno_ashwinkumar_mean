#!/bin/bash

echo "my file name = $0"
echo "number of command line arguments = $#"
echo "command line arguments = $@"
echo "command line arguments = $*"

echo "first command line arguments = $1"
echo "second command line arguments = $2"

echo "last command output = $?"
echo "PID of current program = $$"