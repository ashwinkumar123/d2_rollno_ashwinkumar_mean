#!/bin/bash

# function declaration
# parameterless function
function1() {
  echo "inside function"
}

# function call
# function1


# function declaration
# parameterized 
function2() {
  p1=$1
  echo "inside function2"
  echo "p1 = $p1"
}

# function call
# function2 10


# function declaration
add() {
  p1=$1
  p2=$2

  addition=$(expr $p1 + $p2 )
  echo "addition = $addition"
}

# function call
add 20 40