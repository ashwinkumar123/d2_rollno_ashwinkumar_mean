#!/bin/bash

create_users() {
  # ask user how many users need to be created
  # the number of numbers will be stored in a variable count
  read -p "how many users you want to create ? " count

  echo "creating $count users"

  # traditional for loop
  for (( index=1; index<=$count; index++ ))
  do
    username="user_$index"
    echo "username = $username"

    sudo useradd $username
  done
}

# create_users

delete_users() {
  # ask user how many users need to be created
  # the number of numbers will be stored in a variable count
  read -p "how many users you want to delete ? " count

  echo "creating $count users"

  # traditional for loop
  for (( index=1; index<=$count; index++ ))
  do
    username="user_$index"
    echo "username = $username"

    sudo userdel $username
  done
}

# delete_users