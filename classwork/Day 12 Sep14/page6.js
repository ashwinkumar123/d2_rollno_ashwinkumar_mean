function function1() {
    // array of numbers
    var numbers = [10, 20, 30, 40, 50];
    numbers.push(60);
    numbers.pop();
    console.log(numbers);
    console.log("type of numbers = " + typeof (numbers));
}
// function1()
function function2() {
    // array of numbers
    // const numbers: number[] = [1, 2, 3, 4, 5]
    var numbers = [1, 2, 3, 4, 5];
    // numbers.push(20)
    for (var index = 0; index < numbers.length; index++) {
        var number = numbers[index];
        console.log("square of " + number + " = " + number * number);
    }
}
function2();
