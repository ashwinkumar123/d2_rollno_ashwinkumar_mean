// int num = 100;

let num: number = 100
console.log(`num = ${num}, type = ${typeof(num)}`)

// okay
// num = 100

// not okay
// num = "str"

// string
const firstName: string = "steve"
console.log(`firstName = ${firstName}, type = ${typeof(firstName)}`)

// string
const lastName: string = 'Jobs'
console.log(`lastName = ${lastName}, type = ${typeof(lastName)}`)

// string
const address: string = `
  address line,
  city,
  state
`
console.log(`address = ${address}, type = ${typeof(address)}`)

// boolean
const canVote: boolean = false
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

// undefined
let myVar: undefined
console.log(`myVar = ${myVar}, type = ${typeof(myVar)}`)

// myVar = 100

// object
const person: {name: string, age: number, address: string} 
    = {name: "person1", age: 40, address: "pune"}
console.log(`person = ${person}, type = ${typeof(person)}`)

// object
const mobile: object = {model: "iPhone XS Max"}
console.log(`mobile = ${mobile}, type = ${typeof(mobile)}`)

let result: number | string | boolean
result = 100
result = "error"
result = true

let myvar2: any
myvar2 = 100
myvar2 = "teset"
myvar2 = true
myvar2 = {name: "test"}
myvar2 = undefined