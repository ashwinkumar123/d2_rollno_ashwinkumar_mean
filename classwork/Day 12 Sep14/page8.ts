// variables
// let firstName = "person1"
// let age = 40

// function
// function printInfo() {
// }

class Person {
  // data member of the class (c++)
  // property of the class (TS)
  // fields of the class (Java)
  firstName: string
  age: number
  address: string

  // method
  printInfo() {
    console.log(`first name: ${this.firstName}`)
    console.log(`age: ${this.age}`)
    console.log(`address: ${this.address}`)
  }
    
}

const p1 = new Person()
console.log(p1)
p1.firstName = "steve"
p1.age = 58
p1.address = "USA"
// console.log(p1)
p1.printInfo()


// function Person(firstName, age, address) {
//   this.firstName = firstName
//   this.age = age
//   this.address = address
// }

// const p1 = new Person('person1', 40, 'pune')