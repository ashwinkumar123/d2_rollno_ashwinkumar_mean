"use strict";
exports.__esModule = true;
exports.pi = exports.divide = exports.add = void 0;
function add(p1, p2) {
    console.log("addition = " + (p1 + p2));
}
exports.add = add;
function divide(p1, p2) {
    console.log("division = " + p1 / p2);
}
exports.divide = divide;
exports.pi = 3.14;
