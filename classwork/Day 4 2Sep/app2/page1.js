const mysql = require('mysql')

function getProducts() {
  // step1.1: create connection
  const connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // step1.2: open the connection
  connection.connect()

  // step2: prepare SQL query
  const statement = `select id, title, price, description, category, company from products`

  // step3: execute the query
  // asynchronous
  connection.query(statement, (error, data) => {
    console.log(`error: ${error}`)

    // step4: process the result
    // console.log(data)
    // for (let index = 0; index < data.length; index++) {
    //   const row = data[index];
    
    //   console.log(`id => ${row['id']}`)
    //   console.log(`title => ${row['title']}`)
    //   console.log(`price => ${row['price']}`)
    //   console.log(`description => ${row['description']}`)
    //   console.log(`category => ${row['category']}`)
    //   console.log('----------------------------------------------------')
    // }
    console.table(data)

    // step5: close the connection
    connection.end()
  })

}

// getProducts()

function createProduct(title, description, price, category, userId, company) {
  // step1.1: create connection
  const connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'mydb'
  })

  // step1.2: open the connection
  connection.connect()

  // step2: prepare SQL query
  const statement = `insert into products (title, price, description, category, userId, company) values ('${title}', '${price}', '${description}', '${category}', '${userId}', '${company}')`

  // step3: execute the query
  // asynchronous
  connection.query(statement, (error, data) => {
    console.log(`error: ${error}`)
    console.log(`data: ${data}`)

    // step5: close the connection
    connection.end()
  })

}

// createProduct("product 7", "this is a very nice product", 700, "kitchen", 1, 'lg')
